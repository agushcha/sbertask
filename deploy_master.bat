@ECHO off

ECHO UPDATING REPOSITORY...
ECHO.
git checkout master
git pull origin master

ECHO.
ECHO MIGRATING...
ECHO.
python manage.py migrate

ECHO.
ECHO COLLECTING STATIC...
ECHO.
python manage.py collectstatic --noinput

ECHO.
ECHO DONE
ECHO.
