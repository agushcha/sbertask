@ECHO off

ECHO UPDATING REPOSITORY...
ECHO.
git checkout dev
git pull origin dev

ECHO.
ECHO MIGRATING...
ECHO.
python3.6 manage.py migrate

ECHO.
ECHO COLLECTING STATIC...
ECHO.
python3.6 manage.py collectstatic --noinput

ECHO.
ECHO DONE
ECHO.

pause >nul
REM ECHO starting server...
REM python3.6 manage.py runserver --insecure %IPADDR%
