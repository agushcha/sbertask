from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib import admin


class Developer(models.Model):
    name = models.CharField(max_length=257)
    skills = models.CharField(max_length=128, blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    hours = models.IntegerField(blank=True, null=True)
    author = models.CharField(max_length=257, blank=True, null=True)
    archived = models.NullBooleanField(blank=True, null=True)
    contact_email = models.CharField(max_length=257, blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Task(models.Model):
    title = models.CharField(max_length=257)
    description = models.TextField()
    time = models.IntegerField(blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    author = models.CharField(max_length=256, null=True)
    technologies = models.CharField(max_length=256, null=True)
    author_email = models.CharField(max_length=256, null=True)
    archived = models.NullBooleanField(blank=True, null=True)
    developers = models.ManyToManyField(User, through='Response', related_name='tasks')
    is_responsed = models.CharField(max_length=256, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Response(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='responsed')
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='responsed')
    is_perform = models.NullBooleanField(blank=True, null=True)


class Spec(models.Model):
    title = models.CharField(max_length=257)
    tasks = models.ManyToManyField(Task, related_name='specs')
    users = models.ManyToManyField(User, related_name='specs')

    def __str__(self):
        return self.title
