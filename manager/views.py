from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.utils import timezone
from .models import Task, Developer, Spec, Response
from .forms import DeveloperForm, TaskForm, SpecForm, SignUpForm
from django.db import IntegrityError


def tasks(request):
    tasks = Task.objects.all().exclude(archived=True)
    for task in tasks:
        task.technologies = list()
        task.technologies.extend(task.specs.all())
        task.tags = parseString(task.tags)
    if request.user.is_authenticated:
        try:
            developer = User.objects.get(email=request.user.email)
        except User.DoesNotExist as error:
            print(error)
        else:
            responsed_tasks = developer.responsed.all()
            for response in responsed_tasks:
                response.task.tags = parseString(response.task.tags)
            for task in tasks:
                for response in responsed_tasks:
                    if task.id == response.task.id:
                        task.is_responsed = response.task
            return render(request, 'manager/tasks.html', {'tasks': tasks, 'developer': developer})
    return render(request, 'manager/tasks.html', {'tasks': tasks})


# страница резюме
def developer_new(request):
    specs = Spec.objects.all()
    if request.method == "POST":
        request.POST = request.POST.copy()
        request.POST['created_date'] = timezone.now()
        skills = request.POST.getlist('skills')
        print(request.POST)
        form = DeveloperForm(request.POST)
        if form.is_valid():
            data = form.save()
            data.save()
            for skill_id in skills:
                skill = Spec.objects.get(id=skill_id)
                skill.developers.add(data)
            return redirect('developer_new')
        else:
            error = form.errors
            return render(request, 'manager/developer_page.html', {'error': error, 'specs': specs})
    if request.user.is_authenticated:
        try:
            cv = Developer.objects.all().filter(contact_email=request.user.email)
            for cv_one in cv:
                cv_one.skills = list()
                cv_one.skills.extend(cv_one.specs.all())
            return render(request, 'manager/developer_page.html', {'specs': specs, 'cv': cv})
        except Developer.DoesNotExist:
            cv = None
    return render(request, 'manager/developer_page.html', {'specs': specs})


def delete_card(request, id):
    if request.method == "GET":
        instance = Developer.objects.get(id=id)
        try:
            instance.delete()
            return redirect('developer_new')
        except Developer.DoesNotExist:
            error = "Object not found"
            return redirect('developer_new', {'error': error})
    return render(request, 'manager/developer_page.html', {'specs': specs})


def edit_card(request, id):
    cv = get_object_or_404(Developer, id=id)
    cv.skills = list()
    cv.skills.extend(cv.specs.all())
    specs = Spec.objects.values_list()
    if request.method == "POST":
        skills = request.POST.getlist('skills')
        instance = get_object_or_404(Developer, id=id)
        form = DeveloperForm(request.POST or None, instance=instance)
        if form.is_valid():
            form.save()
            instance.specs.clear()
            for skill_id in skills:
                skill = Spec.objects.get(id=skill_id)
                skill.developers.add(instance)
            return redirect('developer_new')
        else:
            error = form.errors
            return render(request, 'manager/developer_page.html', {'error': error, 'specs': specs})
    return render(request, 'manager/edit_card.html', {'cv': cv, 'specs': specs})
# конец страницы резюме


# Страница добавления задачи
def task_new(request):
    specs = Spec.objects.all()
    request.POST = request.POST.copy()
    technologies = request.POST.getlist('technologies')
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            data = form.save()
            data.save()
            for spec_id in technologies:
                spec = Spec.objects.get(id=spec_id)
                spec.tasks.add(data)
            return redirect('tasks')
        else:
            error = form.errors
            return render(request, 'manager/manager_page.html', {'error': error})
    if request.user.is_authenticated:
        try:
            tasks = Task.objects.filter(author_email=request.user.email)
            for task in tasks:
                task.technologies = list()
                task.technologies.extend(task.specs.all())
                task.tags = parseString(task.tags)
            responses = Response.objects.filter(task__in=tasks)
            return render(request, 'manager/manager_page.html', {'specs': specs, 'tasks': tasks, 'responses': responses })
        except Response.DoesNotExist:
            tasks = None
    return render(request, 'manager/manager_page.html', {'specs': specs})


def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    task.technologies = list()
    task.technologies.extend(task.specs.all())
    specs = Spec.objects.values_list()
    if request.method == "POST":
        technologies = request.POST.getlist('technologies')
        instance = get_object_or_404(Task, id=id)
        form = TaskForm(request.POST or None, instance=instance)
        instance.specs.clear()
        if form.is_valid():
            form.save()
            for technology in technologies:
                tech = Spec.objects.get(id=technology)
                tech.tasks.add(instance)
            return redirect('task_new')
        else:
            error = form.errors
            return render(request, 'manager/manager_page.html', {'error': error, 'specs': specs})
    return render(request, 'manager/edit_task.html', {'task': task, 'specs': specs})


def delete_task(request, id):
    if request.method == "GET":
        instance = Task.objects.get(id=id)
        try:
            instance.delete()
            return redirect('task_new')
        except IntegrityError as error:
            print(error)
            error_msg = "Вы не можете удалить эту задачу, так как на нее есть отклик"
            return redirect('task_new')
# Конец страницы добавления задачи


def developers(request):
    developers = User.objects.all().filter(is_active=True, is_staff=False)
    if request.user.is_authenticated:
        tasks = Task.objects.all().exclude(archived=True).filter(author_email=request.user.email)
    for developer in developers:
        responses = Response.objects.filter(user=developer).values('task').distinct()
        unassigned_tasks = tasks.exclude(id__in=responses)
        setattr(developer, "unassigned_tasks", unassigned_tasks)
    return render(request, 'manager/developers.html', {'developers': developers})


def specs(request):
    specs = Spec.objects.all()
    if request.method == "POST":
        form = SpecForm(request.POST)
        if form.is_valid():
            data = form.save()
            data.save()
            render(request, 'manager/specs.html')
        else:
            error = form.errors
            return render(request, 'manager/specs.html', {'error': error, 'specs': specs})
    return render(request, 'manager/specs.html', {'specs': specs})


def profile(request):
    return render(request, 'registration/profile.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('tasks')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


# Emailings
def manager_hire(request, task_id, employee_id, employer_id):
    if request.method == 'GET':
        task = Task.objects.get(id=task_id)
        employee = User.objects.get(id=employee_id)
        employer = User.objects.get(id=employer_id)
        if task and employee and employer:
            Response.objects.create(user=employee, task=task, is_perform=False)
            return HttpResponse(200)
        else:
            print("Not enough arguments!")
            return HttpResponse(400)


def developer_hire(request, task_id, employee_id):
    if request.method == 'GET':
        employee = User.objects.get(id=employee_id)
        developer = User.objects.get(email=employee.email)
        task = Task.objects.get(id=task_id)
        if task and employee:
            Response.objects.create(user=developer, task=task, is_perform=False)
            return HttpResponse(200)
        else:
            print("Not enough arguments!")
            return HttpResponse(400)


def developer_hire_delete(request, task_id, developer_id):
    if request.method == 'GET':
        task = Task.objects.get(id=task_id)
        developer = User.objects.get(id=developer_id)
        if task:
            Response.objects.get(task=task, user=developer).delete()
            return HttpResponse(200)
        else:
            print('Object not found')
            return HttpResponse(400)


def response_change_perform(request, task_id, developer_id):
    if request.method == 'GET':
        task = Task.objects.get(id=task_id)
        developer = User.objects.get(id=developer_id)
        if task:
            response = Response.objects.get(task=task, user=developer)
            response.is_perform = True
            response.save()
            return HttpResponse(200)
        else:
            print('Object not found')
            return HttpResponse(400)


# показ откликов для девелопера
def show_responses(request):
    try:
        developer = User.objects.get(email=request.user.email)
    except User.DoesNotExist as error:
        responsed_tasks = None
        print(error)
    else:
        responsed_tasks = developer.responsed.all()
        for response in responsed_tasks:
            response.task.technologies = list()
            response.task.technologies.extend(response.task.specs.all())
            response.task.tags = parseString(response.task.tags)
    return render(request, 'manager/responses.html', {'responsed_tasks': responsed_tasks})


# Helpers
def parseTechnologies(array):
    string = ' '.join(array)
    return string


def parseTasks(array):
    for task in array:
        technologies = task.technologies
        tags = task.tags
        task.technologies = parseString(technologies)
        task.tags = parseString(tags)
    return array


def parseString(string):
    array = string.split(' ')
    return array
