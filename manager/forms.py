from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Developer, Task, Spec, User


class DeveloperForm(forms.ModelForm):
    class Meta:
        model = Developer
        fields = ('name', 'about', 'skills', 'hours', 'author', 'contact_email', 'archived')


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'description', 'time', 'tags', 'author', 'author_email', 'archived')


class SpecForm(forms.ModelForm):
    class Meta:
        model = Spec
        fields = ('title',)


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Обязательно.', label='Ваше имя')
    last_name = forms.CharField(max_length=30, required=True, help_text='Обязательно.', label='Ваша фамилия')
    email = forms.EmailField(max_length=254, help_text='Обязательно. Введите корректный адрес эл. почты.', label='Адрес эл. почты')
    skills = forms.ModelMultipleChoiceField(queryset=Spec.objects.all(), label='Навыки')

    def save(self):
        data = self.cleaned_data
        user = User.objects.create_user(username=data['username'], email=data['email'], first_name=data['first_name'],
            last_name=data['last_name'], password=data['password1'])
        skills = data['skills']
        for skill in skills:
            instance = Spec.objects.get(id=skill.id)
            instance.users.add(user)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )
