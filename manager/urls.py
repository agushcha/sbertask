from django.conf.urls import include, url
from django.views.generic import RedirectView
from . import views
from django.conf import settings

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/tasks/')),
    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^tasks/edit/(?P<id>\d+)/$', views.edit_task, name='edit_task'),
    url(r'^tasks/delete/(?P<id>\d+)/$', views.delete_task, name='delete_task'),
    url(r'^tasks/hire/(?P<task_id>\d+)/(?P<employee_id>\d+)/$', views.developer_hire, name='developer_hire'),
    url(r'^tasks/hire/delete/(?P<task_id>\d+)/(?P<developer_id>\d+)$', views.developer_hire_delete, name='developer_hire_delete'),
    url(r'^developer/$', views.developer_new, name='developer_new'),
    url(r'^developer/edit/(?P<id>\d+)/$', views.edit_card, name='edit_card'),
    url(r'^developer/delete/(?P<id>\d+)/$', views.delete_card, name='delete_card'),
    url(r'^manager/$', views.task_new, name='task_new'),
    url(r'^manager/response/(?P<task_id>\d+)/(?P<developer_id>\d+)/$', views.response_change_perform, name='response_change_perform'),
    url(r'^developers/$', views.developers, name='developers'),
    url(r'^developers/hire/(?P<task_id>\d+)/(?P<employee_id>\d+)/(?P<employer_id>\d+)/$', views.manager_hire, name='manager_hire'),
    url(r'^specs/$', views.specs, name='specs'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/profile/$', views.profile, name='profile'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^responses/$', views.show_responses, name='responses'),
]

if settings.DEBUG:
    try:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
    except:
        pass
