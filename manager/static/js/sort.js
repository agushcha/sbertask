$(document).ready(function() {
    $('#sort-link').click(function() {
        var $parent;
        $parent = $('div').filter(function() {
            return this.id.match(/-list$/);
        });
        console.log($parent);
        var $attr = 'hours-value';
        var $outer_div = '.position';
        var $sort_icon = '#sort-icon';
        if($($sort_icon).hasClass('fa-angle-down')) {
            ascSort($attr, $outer_div, $parent)
            $($sort_icon).removeClass('fa-angle-down');
            $($sort_icon).addClass('fa-angle-up');
        } else {
            descSort($attr, $outer_div, $parent)
            $($sort_icon).removeClass('fa-angle-up');
            $($sort_icon).addClass('fa-angle-down');
        }

    });
    function ascSort($attr, $outer_div, $parent) {
        ($parent).find($outer_div).sort(function(a, b) {
            return +a.getAttribute($attr) - +b.getAttribute($attr);
        }).appendTo($parent);
    }
    function descSort($attr, $outer_div, $parent) {
        ($parent).find($outer_div).sort(function(a, b) {
            return +b.getAttribute($attr) - +a.getAttribute($attr);
        }).appendTo($parent);
    }
});
