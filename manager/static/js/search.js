$(document).ready(function() {
    $("#show").click(function() {
        var elements = $('.position').length;
        var hidden = 0;
        if ($("#filterInput").val() != '') {
            $('#show-all').show();
        }
        var txt = $('#filterInput').val();
        $.each($('.position'), function(){
            if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
                $(this).show();
            } else {
                hidden++;
                $(this).hide();
            }
        });
        if (elements == hidden) {
            $('#no-result').show();
        } else {
            $('#no-result').hide();
        }
    });
    $("#show-all").click(function() {
        $('.position').show();
        $("#show-all").hide();
        $('#no-result').hide();
    });
});
