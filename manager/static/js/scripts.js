$(document).ready(function() {
    var $block = '.hours';
    $($block).each(function() {
        var $number = +($(this).html());
        var $titles = ['час', 'часа', 'часов'];
        var $decCache = [];
        var $decCases = [2, 0, 1, 1, 1, 2];
        if(!$decCache[$number]) $decCache[$number] = $number % 100 > 4 && $number % 100 < 20 ? 2 : $decCases[Math.min($number % 10, 5)];
        ($(this).html($number + ' ' + $titles[$decCache[$number]]));
    });
});

$(document).ready(function() {
    $(".delete-event").click(function() {
        return confirm("Вы действительно хотите удалить?");
    });
});

function mailto(to, subject, body_message, callback, response_link) {
    var mailto_link = 'mailto:' + to + '?subject=' + subject + '&body=' + body_message;
    if(document.location = mailto_link) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', callback, true);
        xhr.onload = function () {
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    $(response_link).hide();
                    $('#success-mailto').show();
                }
            }
        };
        xhr.send(null);
    }
}

function delete_response(block, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', callback, true);
    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
                $(block).remove();
                $('#success-delete-hire').show();
            }
        }
    };
    xhr.send();
}

function response_change_perform(block, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', callback, true);
    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
                $(block).removeClass('btn-outline-primary');
                $(block).addClass('btn-success');
                $(block).html('Подтверждено');
            }
        }
    };
    xhr.send();
}

$(document).ready(function() {
    $("#addSpec").click(function() {
        $("#inputSpecs").clone().insertBefore($("#addSpecField"));
    });
    $("#delSpec").click(function() {
        $input = 'div[id=inputSpecs]';
        if ($($input).length > 1) {
            $("#specs").find($input).last().remove();
        }
    });
});
