from django.contrib import admin
from .models import Developer, Task, Spec
admin.site.register(Developer)
admin.site.register(Task)
admin.site.register(Spec)
