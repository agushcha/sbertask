# HRM

## Основное
Environment:
* Проект работает на Python 3.6.4 ([download](https://www.python.org/downloads/release/python-364/))   
* Требуется наличие pip ([download](https://pip.pypa.io/en/stable/installing/))  

Работа с зависимостями:
* Установить зависимости:   
```$ pip install -r requirements.txt```.

## venv
Создать и активировать virtual environment

## Dependencies
* Django v1.11.12 ([Docs](https://docs.djangoproject.com/en/1.11/), [GitHub](https://github.com/django/django), [pypi](https://pypi.org/project/Django/1.11.12/))
* Crispy forms v1.7.2 ([pypi](https://pypi.org/project/django-crispy-forms/1.7.2/))

## Запуск
При первом запуске нужно сделать:  
* `$ python manage.py migrate`

Для обычного запуска:  
`$ python manage.py runserver`

## Сборка
Для сборки статических файлов нужно прописать   
```$ python manage.py collectstatic```

## База данных
По умолчанию стоит **SQLite 3**.

## Деплой
Запустить `deploy_dev.bat` для деплоя **Dev** ветки на тестовом стенде.

## URL приложения
* App - [10.36.7.7:8899](http://10.36.7.7:8899)
